﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;

namespace WindowsFormsApp2
{
    public partial class admin : Form
    {
        Excel.Application excelApp;
        Excel.Workbook exceldoc;
        Excel.Worksheet excelsheet;
        Excel.Worksheet excelsheet2;
        Excel.Worksheet excelsheet3;
        public admin()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void startbase_btn_Click(object sender, EventArgs e)
        {
            log_txt.Text= "log start \n";
            excelApp = new Excel.Application();
            exceldoc = excelApp.Workbooks.Add();
            excelsheet = (Excel.Worksheet)exceldoc.Worksheets.get_Item(1);
            //excelsheet2 = (Excel.Worksheet)exceldoc.Worksheets.get_Item(2);
            //excelsheet3 = (Excel.Worksheet)exceldoc.Worksheets.get_Item(3);
            excelsheet.Cells[1, 1] = "Назва";
            excelsheet.Cells[2, 1] = "Ціна";
            excelsheet.Cells[3, 1] = "Виробник";
            excelsheet.Cells[4, 1] = "Колір";
            excelsheet.Cells[5, 1] = "Об'єм";
            excelsheet.Cells[6, 1] = "Смарт контроль";
            excelsheet.Cells[7, 1] = "Система охолодження";
            excelsheet.Cells[8, 1] = "Тип компресора";
            excelsheet.Cells[9, 1] = "Тип холодильника";
            log_txt.Text += "Base file created \n";
            openbase_btn.Enabled = true;
            red_btn.Enabled = true;
            add_btn.Enabled = true;
            del_btn.Enabled = true;
            saveend_btn.Enabled = true;
            startbase_btn.Enabled = false;
            log_txt.Text += "access: yes \n";
        }

        private void openbase_btn_Click(object sender, EventArgs e)
        {
            if(excelApp.Visible == true)
            {
                excelApp.Visible = false;
                openbase_btn.Text = "Переглянути файл бази";
                log_txt.Text += "visible file: no \n";
            }
            else
            {
                excelApp.Visible = true;
                openbase_btn.Text = "Закрити файл бази";
                log_txt.Text += "visible file: yes \n";
            }
        }

        private void saveend_btn_Click(object sender, EventArgs e)
        {
            try
            {
                excelApp.SaveWorkspace();
            }
            catch (System.Runtime.InteropServices.COMException)
            {
            };
            Process[] List;
            List = Process.GetProcessesByName("EXCEL");
            foreach (Process proc in List)
            {
                proc.Kill();
            };
            log_txt.Text += "base file closed \n";
            openbase_btn.Enabled = false;
            red_btn.Enabled = false;
            add_btn.Enabled = false;
            del_btn.Enabled = false;
            saveend_btn.Enabled = false;
            startbase_btn.Enabled = true;
            log_txt.Text += "access: no \n";
        }

        private void add_btn_Click(object sender, EventArgs e)
        {
            add addfridge = new add(excelApp,exceldoc,excelsheet);
            addfridge.ShowDialog();            
        }

        private void del_btn_Click(object sender, EventArgs e)
        {
            Delete delform = new Delete(excelApp, exceldoc, excelsheet);
            delform.ShowDialog();
        }

        private void red_btn_Click(object sender, EventArgs e)
        {
            Redact redform = new Redact(excelApp, exceldoc, excelsheet);
            redform.ShowDialog();
        }
    }
}
