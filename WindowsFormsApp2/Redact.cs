﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;

namespace WindowsFormsApp2
{
    public partial class Redact : Form
    {
        Excel.Application excelApp;
        Excel.Workbook exceldoc;
        Excel.Worksheet excelsheet;
        //Excel.Worksheet excelsheet2;
        //Excel.Worksheet excelsheet3;
        int n = 2;
        public Redact(Excel.Application excelApp, Excel.Workbook exceldoc, Excel.Worksheet excelsheet)
        {
            InitializeComponent();
            this.excelApp = excelApp;
            this.exceldoc = exceldoc;
            this.excelsheet = excelsheet;
        }

        private void Redact_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                while (excelsheet.Cells[1, n].Value.ToString() != textBox1.Text)
                {
                    n = n + 1;
                }
                MessageBox.Show("Поле знайдено!", "Успіх", MessageBoxButtons.OK, MessageBoxIcon.Information);
                button2.Enabled = true;
            }
            catch (Microsoft.CSharp.RuntimeBinder.RuntimeBinderException)
            {
                n = n + 1;
                if (n == 100)
                {
                    MessageBox.Show("Зацикиклення програми!", "Аварійне завершення", MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Error);
                };
            };
        }

        private void button2_Click(object sender, EventArgs e)
        {
            excelsheet.Cells[2, n] = textBox2.Text;
            excelsheet.Cells[3, n] = textBox3.Text;
            excelsheet.Cells[4, n] = textBox4.Text;
            excelsheet.Cells[5, n] = textBox5.Text;
            excelsheet.Cells[6, n] = comboBox1.Text;
            excelsheet.Cells[7, n] = comboBox2.Text;
            excelsheet.Cells[8, n] = comboBox3.Text;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (textBox1.Text == "")
            {
                button1.Enabled = false;
            }
            else
            {
                button1.Enabled = true;
            }
        }
    }
}
