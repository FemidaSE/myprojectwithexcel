﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;

namespace WindowsFormsApp2
{
    public partial class user : Form
    {
        Excel.Application excelApp;
        Excel.Workbook exceldoc;
        Excel.Worksheet excelsheet;
        //Excel.Workbook exceldoc;
        public user()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            if (ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                textBox1.Text = ofd.FileName;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            int n = 2;
            excelApp = new Excel.Application();
            exceldoc= excelApp.Workbooks.Open(textBox1.Text);
            excelApp.Visible = false;
            //exceldoc = excelApp.Workbooks.Add();
            excelsheet = (Excel.Worksheet)exceldoc.Worksheets.get_Item(1);
            try
            {
                while (excelsheet.Cells[1, n].Value.ToString() != textBox2.Text)
                {
                    n = n + 1;
                }
                MessageBox.Show("Поле знайдено!", "Успіх", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Microsoft.CSharp.RuntimeBinder.RuntimeBinderException)
            {
                n = n + 1;
                if (n == 100)
                {
                    MessageBox.Show("Зацикиклення програми!", "Аварійне завершення", MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Error);
                };
            };
            lname.Text=excelsheet.Cells[1, n].Value.ToString();
            lprice.Text=excelsheet.Cells[2, n].value.ToString();
            lman.Text=excelsheet.Cells[3, n].value.ToString();
            lcolor.Text=excelsheet.Cells[4, n].value.ToString();
            larride.Text=excelsheet.Cells[5, n].value.ToString();
            lsc.Text=excelsheet.Cells[6, n].value.ToString();
            lscol.Text=excelsheet.Cells[7, n].value.ToString();
            ltk.Text=excelsheet.Cells[8, n].value.ToString();
            lf.Text=excelsheet.Cells[9, n].value.ToString();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                excelApp.SaveWorkspace();
            }
            catch (System.Runtime.InteropServices.COMException)
            {
            };
            Process[] List;
            List = Process.GetProcessesByName("EXCEL");
            foreach (Process proc in List)
            {
                proc.Kill();
            };
        }
    }
}
