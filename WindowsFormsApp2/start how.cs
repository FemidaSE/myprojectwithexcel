﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class start_how : Form
    {
        public start_how()
        {
            InitializeComponent();
        }

        private void admin_btn_Click(object sender, EventArgs e)
        {
            admin administrator = new admin();
            administrator.ShowDialog();
        }

        private void user_btn_Click(object sender, EventArgs e)
        {
            user username = new user();
            username.ShowDialog();
        }
    }
}
