﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp2
{
    class fridge
    {
        public string name;
        public double price;
        public string manufacturer;
        public string color;
        public int amount;
        List<onecamers> loc = new List<onecamers>();
        public fridge(string name, double price, string manufacturer, string color, int amount)
        {
            this.name = name;
            this.price = price;
            this.manufacturer = manufacturer;
            this.color = color;
            this.amount = amount;
        }
    }
}
