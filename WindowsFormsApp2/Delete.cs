﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;

namespace WindowsFormsApp2
{
    public partial class Delete : Form
    {
        Excel.Application excelApp;
        Excel.Workbook exceldoc;
        Excel.Worksheet excelsheet;
        public Delete(Excel.Application excelApp, Excel.Workbook exceldoc, Excel.Worksheet excelsheet)
        {
            InitializeComponent();
            this.excelApp = excelApp;
            this.exceldoc = exceldoc;
            this.excelsheet = excelsheet;
        }

        private void Delete_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            int n = 2;
            try
            {
                while (excelsheet.Cells[1, n].Value.ToString() != textBox1.Text)
                {
                    n = n + 1;
                }
            }
            catch (Microsoft.CSharp.RuntimeBinder.RuntimeBinderException)
            {
                n = n + 1;
                if (n == 100)
                {
                    MessageBox.Show("Зацикиклення програми!", "Аварійне завершення", MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Error);
                };
            };
            excelsheet.Cells[1, n] = "";
            excelsheet.Cells[2, n] = "";
            excelsheet.Cells[3, n] = "";
            excelsheet.Cells[4, n] = "";
            excelsheet.Cells[5, n] = "";
            excelsheet.Cells[6, n] = "";
            excelsheet.Cells[7, n] = "";
            excelsheet.Cells[8, n] = "";
            excelsheet.Cells[9, n] = "";
            MessageBox.Show("Поле знайдено і видалено!", "Успіх", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (textBox1.Text == "")
            {
                button1.Enabled = false;
            }
            else
            {
                button1.Enabled = true;
            }
        }
    }
}
