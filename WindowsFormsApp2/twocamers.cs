﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp2
{
    class twocamers:fridge
    {
        public string smart_control;
        public string coldsystem;
        public string typecompressor;
        public string type = "Двокамерний";
        public twocamers(string name, double price, string manufacturer, string color, int amount, string smart_control, string coldsystem, string typecompressor):base(name, price,manufacturer,color,amount)
        {
            this.smart_control = smart_control;
            this.coldsystem = coldsystem;
            this.typecompressor = typecompressor;
        }
    }
}
