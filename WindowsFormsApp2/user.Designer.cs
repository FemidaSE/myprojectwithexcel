﻿namespace WindowsFormsApp2
{
    partial class user
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(user));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.lf = new System.Windows.Forms.Label();
            this.ltk = new System.Windows.Forms.Label();
            this.lscol = new System.Windows.Forms.Label();
            this.lsc = new System.Windows.Forms.Label();
            this.larride = new System.Windows.Forms.Label();
            this.lcolor = new System.Windows.Forms.Label();
            this.lman = new System.Windows.Forms.Label();
            this.lprice = new System.Windows.Forms.Label();
            this.lname = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(265, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(123, 129);
            this.pictureBox1.TabIndex = 7;
            this.pictureBox1.TabStop = false;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(12, 35);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(157, 45);
            this.textBox1.TabIndex = 8;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(175, 32);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 48);
            this.button1.TabIndex = 9;
            this.button1.Text = "Вибрати файл бази";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(12, 107);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(157, 24);
            this.button2.TabIndex = 10;
            this.button2.Text = "Виконати пошук по базі";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.Color.SkyBlue;
            this.label1.Location = new System.Drawing.Point(14, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(236, 19);
            this.label1.TabIndex = 11;
            this.label1.Text = "Вітаю вас в \"Холодильник+!!!\"";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(175, 83);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 48);
            this.button3.TabIndex = 12;
            this.button3.Text = "Закрити базу";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(12, 86);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(157, 18);
            this.textBox2.TabIndex = 13;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 134);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(210, 13);
            this.label2.TabIndex = 14;
            this.label2.Text = "Інформація про вибраний холодильник: ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 147);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 13);
            this.label3.TabIndex = 15;
            this.label3.Text = "Назва";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 160);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 13);
            this.label4.TabIndex = 16;
            this.label4.Text = "Ціна";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(15, 173);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 13);
            this.label5.TabIndex = 17;
            this.label5.Text = "Виробник";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(15, 186);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(34, 13);
            this.label6.TabIndex = 18;
            this.label6.Text = "Колір";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(15, 199);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(37, 13);
            this.label7.TabIndex = 19;
            this.label7.Text = "Об\'єм";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(15, 249);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(100, 13);
            this.label9.TabIndex = 23;
            this.label9.Text = "Тип холодильника";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(15, 236);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(91, 13);
            this.label10.TabIndex = 22;
            this.label10.Text = "Тип компресора";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(15, 223);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(121, 13);
            this.label11.TabIndex = 21;
            this.label11.Text = "Система охолодження";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(15, 210);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(89, 13);
            this.label12.TabIndex = 20;
            this.label12.Text = "Смарт-контроль";
            // 
            // lf
            // 
            this.lf.AutoSize = true;
            this.lf.Location = new System.Drawing.Point(172, 249);
            this.lf.Name = "lf";
            this.lf.Size = new System.Drawing.Size(10, 13);
            this.lf.TabIndex = 32;
            this.lf.Text = "-";
            // 
            // ltk
            // 
            this.ltk.AutoSize = true;
            this.ltk.Location = new System.Drawing.Point(172, 236);
            this.ltk.Name = "ltk";
            this.ltk.Size = new System.Drawing.Size(10, 13);
            this.ltk.TabIndex = 31;
            this.ltk.Text = "-";
            // 
            // lscol
            // 
            this.lscol.AutoSize = true;
            this.lscol.Location = new System.Drawing.Point(172, 223);
            this.lscol.Name = "lscol";
            this.lscol.Size = new System.Drawing.Size(10, 13);
            this.lscol.TabIndex = 30;
            this.lscol.Text = "-";
            // 
            // lsc
            // 
            this.lsc.AutoSize = true;
            this.lsc.Location = new System.Drawing.Point(172, 210);
            this.lsc.Name = "lsc";
            this.lsc.Size = new System.Drawing.Size(10, 13);
            this.lsc.TabIndex = 29;
            this.lsc.Text = "-";
            // 
            // larride
            // 
            this.larride.AutoSize = true;
            this.larride.Location = new System.Drawing.Point(172, 199);
            this.larride.Name = "larride";
            this.larride.Size = new System.Drawing.Size(10, 13);
            this.larride.TabIndex = 28;
            this.larride.Text = "-";
            // 
            // lcolor
            // 
            this.lcolor.AutoSize = true;
            this.lcolor.Location = new System.Drawing.Point(172, 186);
            this.lcolor.Name = "lcolor";
            this.lcolor.Size = new System.Drawing.Size(10, 13);
            this.lcolor.TabIndex = 27;
            this.lcolor.Text = "-";
            // 
            // lman
            // 
            this.lman.AutoSize = true;
            this.lman.Location = new System.Drawing.Point(172, 173);
            this.lman.Name = "lman";
            this.lman.Size = new System.Drawing.Size(10, 13);
            this.lman.TabIndex = 26;
            this.lman.Text = "-";
            // 
            // lprice
            // 
            this.lprice.AutoSize = true;
            this.lprice.Location = new System.Drawing.Point(172, 160);
            this.lprice.Name = "lprice";
            this.lprice.Size = new System.Drawing.Size(10, 13);
            this.lprice.TabIndex = 25;
            this.lprice.Text = "-";
            // 
            // lname
            // 
            this.lname.AutoSize = true;
            this.lname.Location = new System.Drawing.Point(172, 147);
            this.lname.Name = "lname";
            this.lname.Size = new System.Drawing.Size(10, 13);
            this.lname.TabIndex = 24;
            this.lname.Text = "-";
            // 
            // user
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(391, 316);
            this.Controls.Add(this.lf);
            this.Controls.Add(this.ltk);
            this.Controls.Add(this.lscol);
            this.Controls.Add(this.lsc);
            this.Controls.Add(this.larride);
            this.Controls.Add(this.lcolor);
            this.Controls.Add(this.lman);
            this.Controls.Add(this.lprice);
            this.Controls.Add(this.lname);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "user";
            this.Text = "user";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label lf;
        private System.Windows.Forms.Label ltk;
        private System.Windows.Forms.Label lscol;
        private System.Windows.Forms.Label lsc;
        private System.Windows.Forms.Label larride;
        private System.Windows.Forms.Label lcolor;
        private System.Windows.Forms.Label lman;
        private System.Windows.Forms.Label lprice;
        private System.Windows.Forms.Label lname;
    }
}