﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;

namespace WindowsFormsApp2
{
    public partial class add : Form
    {
        Excel.Application excelApp;
        Excel.Workbook exceldoc;
        Excel.Worksheet excelsheet;
        //Excel.Worksheet excelsheet2;
        //Excel.Worksheet excelsheet3;
        List<onecamers> loc = new List<onecamers>();
        List<twocamers> ltc = new List<twocamers>();
        List<threecamers> ltrc = new List<threecamers>();
        int n = 2;
        public add(Excel.Application excelApp,Excel.Workbook exceldoc,Excel.Worksheet excelsheet)
        {
            InitializeComponent();
            this.excelApp = excelApp;
            this.exceldoc = exceldoc;
            this.excelsheet = excelsheet;
        }

        private void add_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (checkBox1.Checked == true)
            {
                loc.Add(new onecamers(textBox1.Text,Convert.ToDouble(textBox2.Text),textBox3.Text,textBox4.Text,Convert.ToInt32(textBox5.Text),comboBox1.Text,comboBox2.Text,comboBox3.Text));
            }
            if (checkBox2.Checked == true)
            {
                ltc.Add(new twocamers(textBox1.Text, Convert.ToDouble(textBox2.Text), textBox3.Text, textBox4.Text, Convert.ToInt32(textBox5.Text), comboBox1.Text, comboBox2.Text, comboBox3.Text));
            }
            if (checkBox3.Checked == true)
            {
                ltrc.Add(new threecamers(textBox1.Text, Convert.ToDouble(textBox2.Text), textBox3.Text, textBox4.Text, Convert.ToInt32(textBox5.Text), comboBox1.Text, comboBox2.Text, comboBox3.Text));
            }
            button2.Enabled = true;
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked == true)
            {
                checkBox2.Enabled = false;
                checkBox3.Enabled = false;
                button1.Enabled = true;
            }
            else
            {
                checkBox2.Enabled = true;
                checkBox3.Enabled = true;
                if((checkBox2.Checked==false) || (checkBox3.Checked == false) || (checkBox1.Checked==false))
                {
                    button1.Enabled = false;
                }
            }
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox2.Checked == true)
            {
                checkBox1.Enabled = false;
                checkBox3.Enabled = false;
                button1.Enabled = true;
            }
            else
            {
                checkBox1.Enabled = true;
                checkBox3.Enabled = true;
                if ((checkBox2.Checked == false) || (checkBox3.Checked == false) || (checkBox1.Checked == false))
                {
                    button1.Enabled = false;
                }
            }
        }

        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox3.Checked == true)
            {
                checkBox2.Enabled = false;
                checkBox1.Enabled = false;
                button1.Enabled = true;
            }
            else
            {
                checkBox2.Enabled = true;
                checkBox1.Enabled = true;
                if ((checkBox2.Checked == false) || (checkBox3.Checked == false) || (checkBox1.Checked == false))
                {
                    button1.Enabled = false;
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            foreach(onecamers var in loc)
            {
                excelsheet.Cells[1, n] = var.name;
                excelsheet.Cells[2, n] = var.price;
                excelsheet.Cells[3, n] = var.manufacturer;
                excelsheet.Cells[4, n] = var.color;
                excelsheet.Cells[5, n] = var.amount;
                excelsheet.Cells[6, n] = var.smart_control;
                excelsheet.Cells[7, n] = var.coldsystem;
                excelsheet.Cells[8, n] = var.typecompressor;
                excelsheet.Cells[9, n] = var.type;
                n = n + 1;
            }
            foreach (twocamers var in ltc)
            {
                excelsheet.Cells[1, n] = var.name;
                excelsheet.Cells[2, n] = var.price;
                excelsheet.Cells[3, n] = var.manufacturer;
                excelsheet.Cells[4, n] = var.color;
                excelsheet.Cells[5, n] = var.amount;
                excelsheet.Cells[6, n] = var.smart_control;
                excelsheet.Cells[7, n] = var.coldsystem;
                excelsheet.Cells[8, n] = var.typecompressor;
                excelsheet.Cells[9, n] = var.type;
                n = n + 1;
            }
            foreach (threecamers var in ltrc)
            {
                excelsheet.Cells[1, n] = var.name;
                excelsheet.Cells[2, n] = var.price;
                excelsheet.Cells[3, n] = var.manufacturer;
                excelsheet.Cells[4, n] = var.color;
                excelsheet.Cells[5, n] = var.amount;
                excelsheet.Cells[6, n] = var.smart_control;
                excelsheet.Cells[7, n] = var.coldsystem;
                excelsheet.Cells[8, n] = var.typecompressor;
                excelsheet.Cells[9, n] = var.type;
                n = n + 1;
            }
            loc.Clear();
            ltc.Clear();
            ltrc.Clear();
        }
    }
}
