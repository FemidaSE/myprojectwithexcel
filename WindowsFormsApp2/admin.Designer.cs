﻿namespace WindowsFormsApp2
{
    partial class admin
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(admin));
            this.label1 = new System.Windows.Forms.Label();
            this.startbase_btn = new System.Windows.Forms.Button();
            this.red_btn = new System.Windows.Forms.Button();
            this.del_btn = new System.Windows.Forms.Button();
            this.add_btn = new System.Windows.Forms.Button();
            this.saveend_btn = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.openbase_btn = new System.Windows.Forms.Button();
            this.log_txt = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.Color.SkyBlue;
            this.label1.Location = new System.Drawing.Point(46, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(468, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "Вітаю вас в системі обліку \"Холодильник+!!!\"";
            // 
            // startbase_btn
            // 
            this.startbase_btn.Location = new System.Drawing.Point(16, 36);
            this.startbase_btn.Name = "startbase_btn";
            this.startbase_btn.Size = new System.Drawing.Size(134, 55);
            this.startbase_btn.TabIndex = 1;
            this.startbase_btn.Text = "Запустити базу";
            this.startbase_btn.UseVisualStyleBackColor = true;
            this.startbase_btn.Click += new System.EventHandler(this.startbase_btn_Click);
            // 
            // red_btn
            // 
            this.red_btn.Enabled = false;
            this.red_btn.Location = new System.Drawing.Point(156, 158);
            this.red_btn.Name = "red_btn";
            this.red_btn.Size = new System.Drawing.Size(134, 55);
            this.red_btn.TabIndex = 2;
            this.red_btn.Text = "Редагувати інформацію про холодильник";
            this.red_btn.UseVisualStyleBackColor = true;
            this.red_btn.Click += new System.EventHandler(this.red_btn_Click);
            // 
            // del_btn
            // 
            this.del_btn.Enabled = false;
            this.del_btn.Location = new System.Drawing.Point(156, 97);
            this.del_btn.Name = "del_btn";
            this.del_btn.Size = new System.Drawing.Size(134, 55);
            this.del_btn.TabIndex = 3;
            this.del_btn.Text = "Видалити холодильник";
            this.del_btn.UseVisualStyleBackColor = true;
            this.del_btn.Click += new System.EventHandler(this.del_btn_Click);
            // 
            // add_btn
            // 
            this.add_btn.Enabled = false;
            this.add_btn.Location = new System.Drawing.Point(156, 36);
            this.add_btn.Name = "add_btn";
            this.add_btn.Size = new System.Drawing.Size(134, 55);
            this.add_btn.TabIndex = 4;
            this.add_btn.Text = "Додати холодильник";
            this.add_btn.UseVisualStyleBackColor = true;
            this.add_btn.Click += new System.EventHandler(this.add_btn_Click);
            // 
            // saveend_btn
            // 
            this.saveend_btn.Enabled = false;
            this.saveend_btn.Location = new System.Drawing.Point(16, 158);
            this.saveend_btn.Name = "saveend_btn";
            this.saveend_btn.Size = new System.Drawing.Size(134, 55);
            this.saveend_btn.TabIndex = 5;
            this.saveend_btn.Text = "Закрити і зберегти базу";
            this.saveend_btn.UseVisualStyleBackColor = true;
            this.saveend_btn.Click += new System.EventHandler(this.saveend_btn_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(296, 36);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(249, 234);
            this.pictureBox1.TabIndex = 6;
            this.pictureBox1.TabStop = false;
            // 
            // openbase_btn
            // 
            this.openbase_btn.Enabled = false;
            this.openbase_btn.Location = new System.Drawing.Point(16, 97);
            this.openbase_btn.Name = "openbase_btn";
            this.openbase_btn.Size = new System.Drawing.Size(134, 55);
            this.openbase_btn.TabIndex = 8;
            this.openbase_btn.Text = "Переглянути файл бази";
            this.openbase_btn.UseVisualStyleBackColor = true;
            this.openbase_btn.Click += new System.EventHandler(this.openbase_btn_Click);
            // 
            // log_txt
            // 
            this.log_txt.Location = new System.Drawing.Point(156, 219);
            this.log_txt.Multiline = true;
            this.log_txt.Name = "log_txt";
            this.log_txt.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.log_txt.Size = new System.Drawing.Size(134, 45);
            this.log_txt.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 27.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.ForeColor = System.Drawing.Color.SkyBlue;
            this.label2.Location = new System.Drawing.Point(22, 219);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(108, 43);
            this.label2.TabIndex = 9;
            this.label2.Text = "LOG:";
            // 
            // admin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(551, 276);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.openbase_btn);
            this.Controls.Add(this.log_txt);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.saveend_btn);
            this.Controls.Add(this.add_btn);
            this.Controls.Add(this.del_btn);
            this.Controls.Add(this.red_btn);
            this.Controls.Add(this.startbase_btn);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "admin";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Admin";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button startbase_btn;
        private System.Windows.Forms.Button red_btn;
        private System.Windows.Forms.Button del_btn;
        private System.Windows.Forms.Button add_btn;
        private System.Windows.Forms.Button saveend_btn;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button openbase_btn;
        private System.Windows.Forms.TextBox log_txt;
        private System.Windows.Forms.Label label2;
    }
}

